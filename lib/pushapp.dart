
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'dart:io';






void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();



  @override
  void initState() {
    super.initState();
    this.iniciarFirebaseListeners();
  }

  void iniciarFirebaseListeners() {

    if (Platform.isIOS) requisitarPermissoesParaNotificacoesNoIos();

    _firebaseMessaging.getToken().then((token){
      print("Firebase token " + token);

    });

    _firebaseMessaging.configure(


      onMessage: (Map<String, dynamic> message) async {
        print('mensagem recebida $message');

        showDialog(context: context,
            builder: (context) =>
                AlertDialog(
                  content: ListTile(
                    title: Text( message['notification']['title'] ),
                    subtitle: Text( message['notification']['body'] ),
                  ),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text('Ok')
                    )
                  ],
                )
        );

      },

      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        return Container(color: Colors.red,);



      },

      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
        return Container(color: Colors.blueAccent,);
      },
    );

  }


  void requisitarPermissoesParaNotificacoesNoIos() {

    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        )
    );
  }



}